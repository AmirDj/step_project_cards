const token = "5db4c838-6625-4594-b0ea-4feb25002fe6";
let authorized = false;
let visitsCollection = [];

class Login {
  constructor() {
    this.token = null;
    this.loggedIn = false;
    this.loginButton = document.getElementById("loginButton");
    this.createVisitButton = document.getElementById("createVisitButton");
    this.logoutButton = document.getElementById("logout");
    this.buttonContainer = document.querySelector(".button");
    this.initialize();
    this.checkAuthorization();
  }

  initialize() {
    this.logoutButton.addEventListener("click", () => this.logout());
    this.loginButton.addEventListener("click", () => this.openModal());
    document
      .getElementById("closeLoginButton")
      .addEventListener("click", () => this.closeModal());
    document
      .getElementById("loginSubmit")
      .addEventListener("click", () => this.handleLogin());
  }

  openModal() {
    const modal = document.getElementById("loginModal");
    modal.style.display = "block";
  }

  closeModal() {
    const modal = document.getElementById("loginModal");
    modal.style.display = "none";
  }

  handleLogin() {
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    if (this.validateEmail(email) && this.validatePassword(password)) {
      this.closeModal();

      this.token = "5db4c838-6625-4594-b0ea-4feb25002fe6";
      this.loggedIn = true;
      authorized = true;


      localStorage.setItem('authorized', 'true');

      this.updateButtonStyles();
    } else {
      alert("Невірна електронна пошта або пароль. Спробуйте ще раз.");
    }
  }

  checkAuthorization() {
    const isAuthorized = localStorage.getItem('authorized');

    if (isAuthorized === 'true') {
      this.updateButtonStyles();
    }
  }

  validateEmail(email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  validatePassword(password) {
    return password.length >= 6;
  }

  logout() {
    this.createVisitButton.style.display = "none";
    this.logoutButton.style.display = "none";
    this.loginButton.style.display = "block";
    authorized = false;
    localStorage.setItem('authorized', 'false');
  }

  updateButtonStyles() {
    this.loginButton.style.display = "none";
    this.createVisitButton.style.display = "block";
    this.logoutButton.style.display = "block";
  }
}

const loginInstance = new Login();

class Modal {
  constructor() {
    this.btnCreate = document.querySelector("#btnCreate");
    this.modal = document.querySelector(".content-modal");
    this.btnCreateVisit = document.querySelector(".create-visit-btn");

    this.openForm();
    this.createForm();
  }

  openForm() {
    this.btnCreateVisit.addEventListener("click", (e) => {
      this.visabilityModalWindow();
    });
  }

  createForm() {
    const formHtml = `
            <div class="modalVisit">
                <div class="modalVisit-content">
                    <div class="modalVisit-header">
                    <h2 class="modalVisit-title">Новий візит</h2>
                  
                    </div>                
                    <form class="createVisit">
                        <div class="mb-3">
                            <input name='title' class="form-control" id ="title" type="text" placeholder="Мета візиту" aria-label="default input example" required/>
                        </div>
                        <div class="mb-3">
                            <textarea name='description' class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Короткий опис візиту" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Терміновість</label>
                            <select name='priory' class="form-control" id ="urgency" aria-label="Default select example" required>
                                <option value="Звичайна">Звичайна</option>
                                <option value="Пріоритетна">Пріоритетна</option>
                                <option value="Невідкладна">Невідкладна</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <input name='firstname' class="form-control" id ="name" type="text" placeholder="ПІБ" aria-label="default input example" required/>
                        </div>
                        <div class="mb-3">
                            <select name='doctor' id ="doctor" class="form-control select-form" aria-label="Default select example" required>
                            <option value="Виберіть лікаря" selected>Виберіть лікаря</option>
                                <option value="Кардіолог">Кардіолог</option>
                                <option value="Стоматолог">Стоматолог</option>
                                <option value="Терапевт">Терапевт</option>
                            </select>
                        </div>
                        <div class="select-output"></div>

                        <div class="btns-modal"> 
                            <button class= "btn-closed" id ="btnDelete">Закрити</button>
                            <button  class="btn-create" id ="btnCreate">Створити</button>
                        </div>                        
                    </form>
                </div>
            </div>`;
    this.modal.insertAdjacentHTML("afterbegin", formHtml);

    document.querySelector("#doctor").addEventListener("change", (e) => {
      this.selectDoctor(e.target.value);
    });

    btnCreate.addEventListener("click", (e) => {
      e.preventDefault();
      if (this.checkform()) {
        this.sendForm();
      } else {
        alert("Please fill all required fields");
      }
    });
    const btnDelete = document.querySelector("#btnDelete");
    btnDelete.addEventListener("click", () => {
      this.visabilityModalWindow();
    });

    document.querySelector(".modalVisit").addEventListener("click", (e) => {
      if (e.target.classList.contains("modalVisit")) {
        this.visabilityModalWindow();
      }
    });
  }

  visabilityModalWindow() {
    this.modal.classList.toggle("active-visit");
  }

  selectDoctor(e) {
    const fildsTherapist = `
         <div class="therapic">
         <div class="mb-3">
             <input class="form-control" id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
         </div>
        </div>`;

    const fildsDentist = `
        <div class="dentic">
            <div class="mb-3">
                <label class="form-label">Дата останнього відвідування</label>
                <input class="form-control" id ="data" type="date" name="date" aria-label="default input example" required/>
            </div>
        </div>`;

    const fildsCardiologist = `
        <div class="heart">
            <div class="mb-3">
                <input name="pressure" id ="pressure" type="number" class="form-control" type="text" placeholder="Звичайний тиск" max="250" aria-label="default input example" required/>
            </div>
            <div class="mb-3">
                <input class="form-control" id ="bmi" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
            </div>
            <div class="mb-3">
                <textarea name="diseases" id ="disease" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
            </div>
            <div class="mb-3">
            <input class="form-control" id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
            </div>
        </div>`;

    const selectOutput = document.querySelector(".select-output");
    selectOutput.innerHTML = "";
    if (e === "Стоматолог") {
      selectOutput.insertAdjacentHTML("afterbegin", fildsDentist);
    } else if (e === "Кардіолог") {
      selectOutput.insertAdjacentHTML("afterbegin", fildsCardiologist);
    } else if (e === "Терапевт") {
      selectOutput.insertAdjacentHTML("afterbegin", fildsTherapist);
    }
  }

  checkform() {
    const inputs = document.querySelectorAll(".form-control");
    const isFormValid = [...inputs].some(item => item.value == "" || item.value == null);
    return !isFormValid
  }

  sendForm() {

    document.querySelector(".createVisit").addEventListener("click", sendingForm);
    
    function sendingForm(e) {
        e.preventDefault();
        const formData = new FormData(this);
        let object = {};
        let status = {
          status: "Open",
        };

        let dataForm = Object.assign(object, status);

        formData.forEach((value, key) => {
          object[key] = value;
        });
        const json = JSON.stringify(dataForm);

        fetch("https://ajax.test-danit.com/api/v2/cards", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: json,
        })
          .then((response) => response.json())
          // .then((response)=> console.log(response))
          .then(() =>
            document.querySelector(".content-modal").classList.remove("active-visit"),
            document.querySelector(".createVisit").reset(),
            document.querySelector(".createVisit").removeEventListener("click", sendingForm)
          )
          .catch((err) => {
            alert("Something went wrong");
            console.error(err);
          });
      };
  }
}

const modal = new Modal();

fetch("https://ajax.test-danit.com/api/v2/cards", {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  },
})
  .then((response) => response.json())
  .then((response) => console.log(response));
  const BoardDiv = document.getElementById("visitsBoard"); //Константа рабочей секции кода

  function CreateRow(Id) //Фунция для добавления рядка с лишками
  {
    const row = document.createElement("div")
    row.classList.add("cards__row");
    row.id = "cards__row " + Id;
    BoardDiv.appendChild(row);
    const list = document.createElement("ul");
    list.classList.add("cards__list");
    for(let i=1; i<=4; i++) //Добавление в список четырех лишек
    {
      const liEl = document.createElement("li");
      liEl.classList.add("cards__list-li");
      liEl.id = "cards__li " + ((Id-1)*4+i);
      list.appendChild(liEl);
    }
    row.appendChild(list);
  }
  function CreateCard(liId) //Функция для создания карточки
  {
    const card__card_html = `
    <div class="cards__card">
    <div class="cards__description">
        <div class="cards__buttons">
            <ul class="cards__buttons-list">
                <li>
                    <button class="cards__button" onclick="editButton()">
                        <p class="cards__button-text">edit</p>
                    </button>
                </li>
                <li>
                    <button class="cards__button" onclick="deletingCard()">
                        <p class="cards__button-text">delete</p>
                    </button>
                </li>
            </ul>
        </div>
        <div class="cards__details">
            <p class="cards__details-text"><b>Врач:</b> <span id="doctor"></span></p>
            <p class="cards__details-text"><b>ФИО:</b> <span id="Name"></span></p>
            <p class="cards__details-text"><b>Срочность:</b> <span id="urgency"></span></p>
            <p class="cards__details-text"><b>Цель встречи:</b> <span id="meetingGoal"></span></p>
            <p class="cards__details-text"><b>Короткое описание встречи:</b> <span id="meetingDescription"></span></p>
          </div>
        <div class="cards__more">
            <button class="cards__more-button" onclick="read_more()">
                more details
            </button>
        </div>
    </div>
  </div>`
  
    const liEl = document.getElementById("cards__li " + liId);
    liEl.insertAdjacentHTML("beforeend", card__card_html);
  }
  function CreateHtml(number) //Функция для создания определенного создания карточек
  {
    const rowsAmount = Math.floor(number/4)+1; //Расчет необходимого количества рядков
    for(let i=1;i<=rowsAmount;i++)
    {
      CreateRow(i); //Создание количества рядов для последующих карточек
    }
    for(let i=1;i<=number;i++)
    {
      CreateCard(i); //Создание самих карточек
    }
  }
  const filtredArr = [];
  function render(array) { //Функция загрузки карточек на сайт
    CreateHtml(array.length);
        for (let i = 0; i < array.length; i++) { //Установление параметров для каждой карточки
          filtredArr[i] = array[i];
          const card = document.getElementById("cards__li " + (i + 1)); 
          const doctorSpan = card.querySelector("#doctor");
          const doctorNameSpan = card.querySelector("#Name");
          const urgencySpan = card.querySelector("#urgency");
          const meetingGoalSpan = card.querySelector("#meetingGoal");
          const meetingDescriptionSpan = card.querySelector("#meetingDescription");
            doctorSpan.textContent = array[i].doctor;
            doctorNameSpan.textContent = array[i].firstname;
            urgencySpan.textContent = array[i].priory;
            meetingGoalSpan.textContent = array[i].title;
            meetingDescriptionSpan.textContent = array[i].age;
        }
  }
  function clear() { //Функция очищения странички от карточек
      const row = document.querySelectorAll(".cards__row");
      for(let i = 0; i < row.length; i++)
      {
        row[i].remove();
      }
  }
  function deletingCard() //Функция удаления карточки
  {
    const thisElement = event.target;
    let parent = thisElement.parentNode;
  while (parent.id == "") { //Поиск лишки с айди карты
    parent = parent.parentNode; 
  }
  const cardElement = parent.id;
  const splitedId = cardElement.split(" ");
  const Cardid = splitedId[1];
  const id = filtredArr[Cardid-1].id; //назначение конкретного айди
  const url = "https://ajax.test-danit.com/api/v2/cards/" + id;
    fetch(url, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
  .then((response) => {
    response.urge
    if (response.ok) {
      console.log("success deleting");
    } else {
      console.log("error:", response.statusText);
    }
  });
  filtredArr.splice(Cardid-1, 1); //Удаления из массива отфильтрованных карточек
  resetPage();
  }
  
  function resetPage() //Обновление карточек после удаления|изменения карточки
  {
    clear();
    render(filtredArr);
  }
  
  function read_more()//Функция чтения карточки
  {
    const thisElement = event.target;
    let parent = thisElement.parentNode;
  while (parent.id == "") {
    parent = parent.parentNode;
  }
  const cardElement = parent.id;
  const splitedId = cardElement.split(" ");
  const Cardid = splitedId[1];
  const id = filtredArr[Cardid-1].id;
  const thisCard = filtredArr[Cardid-1];
  
    const titleTemp = thisCard.title;
    const descriptionTemp = thisCard.description;
  
    let isPrio = "";
    let isBasic = "";
    let isReal = "";
  
    if(thisCard.priory == "Звичайна")
    {
      isPrio = "selected";
    }
    else if(thisCard.priory == "Пріоритетна")
    {
      isBasic = "selected";
    }
    else
    {
      isReal = "selected";
    }
    
    let firstName = thisCard.firstname;
    
    let isDanti = "";
    
    let isCardio = "";
    let weight = "";
    let heartTroubles = "";
    let desiase = "";
  
    let isTera = "";
    console.log(thisCard);
    let ageInp = thisCard.age;
  
    let carDpressure = thisCard.perssure;
    if(carDpressure == null)
    carDpressure = "";
  
    
  
    let lastDate = thisCard.date;
    if(lastDate == null)
    lastDate = "";
  
    console.log(thisCard.doctor);
    if(thisCard.doctor == "Стоматолог")
    {
      isDanti = "selected";
    }
    else if(thisCard.doctor == "Кардіолог")
    { 
      isCardio = "selected";
    }
    else
    {
      isTera = "selected";
      
    }
  
    const formHtml = `
              <div class="modalVisit">
                  <div class="modalVisit-content">
                      <div class="modalVisit-header">
                      <h2 class="modalVisit-title">Новий візит</h2>
                    
                      </div>                
                      <form class="createVisit">
                          <div class="mb-3">
                              <input disabled name='title' value= ${titleTemp} class="form-control" id ="title" type="text" placeholder="Мета візиту" aria-label="default input example" required/>
                          </div>
                          <div class="mb-3">
                              <textarea disabled name='description' class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Короткий опис візиту" required>${descriptionTemp}</textarea>
                          </div>
                          <div class="mb-3">
                              <label class="form-label">Терміновість</label>
                              <select disabled name='priory' class="form-control" id ="urgency" aria-label="Default select example" required>
                                  <option disabled ${isBasic} value="Звичайна">Звичайна</option>
                                  <option disabled ${isPrio} value="Пріоритетна">Пріоритетна</option>
                                  <option disabled ${isReal} value="Невідкладна">Невідкладна</option>
                              </select>
                          </div>
                          <div class="mb-3">
                              <input disabled name='firstname' value= ${firstName} class="form-control" id ="name" type="text" placeholder="ПІБ" aria-label="default input example" required/>
                          </div>
                          <div class="mb-3">
                              <select disabled name='doctor' id ="doctor" selected class="form-control select-form" aria-label="Default select example" required>
                              <option disabled value="Виберіть лікаря" >Виберіть лікаря</option>
                                  <option disabled ${isCardio} value="Кардіолог">Кардіолог</option>
                                  <option disabled ${isDanti} value="Стоматолог">Стоматолог</option>
                                  <option disabled ${isTera} value="Терапевт">Терапевт</option>
                              </select>
                          </div>
                          <div class="select-output"></div>
  
                          <div class="btns-modal"> 
                              <button class= "btn-closed" id ="btnDelete">Закрити</button>
                          </div>                        
                      </form>
                  </div>
              </div>`;
      document.body.insertAdjacentHTML("afterbegin", formHtml);
  
    selectDoctor(thisCard.doctor);
  
      function selectDoctor(e) {
        let fildsTherapist=``;
        let fildsDentist = ``;
        let fildsCardiologist = ``;
        if(thisCard.doctor == "Терапевт")
        {
          fildsTherapist = `
           <div class="therapic">
           <div class="mb-3">
               <input disabled class="form-control" value= ${ageInp} id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
           </div>
          </div>`;
        }
        else
        {
          fildsTherapist = `
           <div class="therapic">
           <div class="mb-3">
               <input disabled class="form-control" id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
           </div>
          </div>`;
        }
      
  
        if(thisCard.doctor == "Стоматолог")
        {
          fildsDentist = `
          <div class="dentic">
              <div class="mb-3">
                  <label class="form-label">Дата останнього відвідування</label>
                  <input disabled class="form-control" value=${lastDate} id ="data" type="date" name="date" aria-label="default input example" required/>
              </div>
          </div>`;
        }
        else
        {
          fildsDentist = `
          <div class="dentic">
              <div class="mb-3">
                  <label class="form-label">Дата останнього відвідування</label>
                  <input disabled class="form-control" id ="data" type="date" name="date" aria-label="default input example" required/>
              </div>
          </div>`;
        }
      
        if(thisCard.doctor == "Кардіолог")
        {
          fildsCardiologist = `
          <div class="heart">
              <div class="mb-3">
                  <input disabled name="pressure" value= ${carDpressure} id ="pressure" class="form-control" type="text" placeholder="Звичайний тиск" max="300" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <input disabled class="form-control" value= ${weight} id ="bmi" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <textarea disabled name="diseases" value= ${heartTroubles} id ="disease" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
              </div>
              <div class="mb-3">
              <input disabled class="form-control" value= ${ageInp} id ="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
              </div>
          </div>`;
        }
        else
        {
          fildsCardiologist = `
          <div class="heart">
              <div class="mb-3">
                  <input disabled name="pressure" id ="pressure" class="form-control" type="text" placeholder="Звичайний тиск" max="300" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <input disabled class="form-control" id ="bmi" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <textarea disabled name="diseases" id ="disease" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
              </div>
              <div class="mb-3">
              <input disabled class="form-control" id ="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
              </div>
          </div>`;
        }
  
      const selectOutput = document.querySelector(".select-output");
      selectOutput.innerHTML = "";
      if (e === "Стоматолог") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsDentist);
      } else if (e === "Кардіолог") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsCardiologist);
      } else if (e === "Терапевт") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsTherapist);
      }
    }
  }
  
  
  let tempId;
  function editButton()//Функция редактирования карточки
  {
    const thisElement = event.target;
    let parent = thisElement.parentNode;
  while (parent.id == "") {
    parent = parent.parentNode;
  }
  const cardElement = parent.id;
  const splitedId = cardElement.split(" ");
  const Cardid = splitedId[1];
  const id = filtredArr[Cardid-1].id;
  tempId = id;
  console.log(tempId);
  const thisCard = filtredArr[Cardid-1];
  
    const titleTemp = thisCard.title;
    const descriptionTemp = thisCard.description;
  
    let isPrio = "";
    let isBasic = "";
    let isReal = "";
  
    if(thisCard.priory == "Звичайна")
    {
      isPrio = "selected";
    }
    else if(thisCard.priory == "Пріоритетна")
    {
      isBasic = "selected";
    }
    else
    {
      isReal = "selected";
    }
    
    let firstName = thisCard.firstname;
    
    let isDanti = "";
    
    let isCardio = "";
    let weight = "";
    let heartTroubles = "";
    let desiase = "";
  
    let isTera = "";
    console.log(thisCard);
    let ageInp = thisCard.age;
  
    let carDpressure = thisCard.perssure;
    if(carDpressure == null)
    carDpressure = "";
  
    
  
    let lastDate = thisCard.date;
    if(lastDate == null)
    lastDate = "";
  
    console.log(thisCard.doctor);
    if(thisCard.doctor == "Стоматолог")
    {
      isDanti = "selected";
    }
    else if(thisCard.doctor == "Кардіолог")
    { 
      isCardio = "selected";
    }
    else
    {
      isTera = "selected";
      
    }
  
    const formHtml = `
              <div class="modalVisit">
                  <div class="modalVisit-content">
                      <div class="modalVisit-header">
                      <h2 class="modalVisit-title">Новий візит</h2>
                    
                      </div>                
                      <form class="createVisit">
                          <div class="mb-3">
                              <input name='title' value= ${titleTemp} class="form-control" id ="title" type="text" placeholder="Мета візиту" aria-label="default input example" required/>
                          </div>
                          <div class="mb-3">
                              <textarea name='description' class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Короткий опис візиту" required>${descriptionTemp}</textarea>
                          </div>
                          <div class="mb-3">
                              <label class="form-label">Терміновість</label>
                              <select name='priory' class="form-control" id ="urgency" aria-label="Default select example" required>
                                  <option ${isBasic} value="Звичайна">Звичайна</option>
                                  <option ${isPrio} value="Пріоритетна">Пріоритетна</option>
                                  <option ${isReal} value="Невідкладна">Невідкладна</option>
                              </select>
                          </div>
                          <div class="mb-3">
                              <input name='firstname' value= ${firstName} class="form-control" id ="name" type="text" placeholder="ПІБ" aria-label="default input example" required/>
                          </div>
                          <div class="mb-3">
                              <select name='doctor' id ="doctor" selected class="form-control select-form" aria-label="Default select example" required>
                              <option value="Виберіть лікаря" >Виберіть лікаря</option>
                                  <option ${isCardio} value="Кардіолог">Кардіолог</option>
                                  <option ${isDanti} value="Стоматолог">Стоматолог</option>
                                  <option ${isTera} value="Терапевт">Терапевт</option>
                              </select>
                          </div>
                          <div class="select-output"></div>
  
                          <div class="btns-modal"> 
                              <button class= "btn-closed" id ="btnDelete">Закрити</button>
                              <button  class="btn-create" onclick="editFetch()">Редактировать</button>
                          </div>                        
                      </form>
                  </div>
              </div>`;
      document.body.insertAdjacentHTML("afterbegin", formHtml);
      const btnCreate = document.querySelector("#btnCreate");
  
      
    selectDoctor(thisCard.doctor);
  
      document.querySelector("#doctor").addEventListener("change", (e) => {
        selectDoctor(e.target.value);
      });
  
      function selectDoctor(e) {
        let fildsTherapist=``;
        let fildsDentist = ``;
        let fildsCardiologist = ``;
        if(thisCard.doctor == "Терапевт")
        {
          fildsTherapist = `
           <div class="therapic">
           <div class="mb-3">
               <input class="form-control" value= ${ageInp} id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
           </div>
          </div>`;
        }
        else
        {
          fildsTherapist = `
           <div class="therapic">
           <div class="mb-3">
               <input class="form-control" id="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
           </div>
          </div>`;
        }
      
  
        if(thisCard.doctor == "Стоматолог")
        {
          fildsDentist = `
          <div class="dentic">
              <div class="mb-3">
                  <label class="form-label">Дата останнього відвідування</label>
                  <input class="form-control" value=${lastDate} id ="data" type="date" name="date" aria-label="default input example" required/>
              </div>
          </div>`;
        }
        else
        {
          fildsDentist = `
          <div class="dentic">
              <div class="mb-3">
                  <label class="form-label">Дата останнього відвідування</label>
                  <input class="form-control" id ="data" type="date" name="date" aria-label="default input example" required/>
              </div>
          </div>`;
        }
      
        if(thisCard.doctor == "Кардіолог")
        {
          fildsCardiologist = `
          <div class="heart">
              <div class="mb-3">
                  <input name="pressure" value= ${carDpressure} id ="pressure" class="form-control" type="text" placeholder="Звичайний тиск" max="300" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <input class="form-control" value= ${weight} id ="bmi" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <textarea name="diseases" value= ${heartTroubles} id ="disease" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
              </div>
              <div class="mb-3">
              <input class="form-control" value= ${ageInp} id ="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
              </div>
          </div>`;
        }
        else
        {
          fildsCardiologist = `
          <div class="heart">
              <div class="mb-3">
                  <input name="pressure" id ="pressure" class="form-control" type="text" placeholder="Звичайний тиск" max="300" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <input class="form-control" id ="bmi" type="number" placeholder="Індекс маси тіла" name="weight" min="3" max="150" aria-label="default input example" required/>
              </div>
              <div class="mb-3">
                  <textarea name="diseases" id ="disease" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Перенесені захворювання серцево-судинної системи" required></textarea>
              </div>
              <div class="mb-3">
              <input class="form-control" id ="age" type="number" placeholder="Вік" name="age" min="1" max="150" aria-label="default input example" required/>
              </div>
          </div>`;
        }
      const selectOutput = document.querySelector(".select-output");
      selectOutput.innerHTML = "";
      if (e === "Стоматолог") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsDentist);
      } else if (e === "Кардіолог") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsCardiologist);
      } else if (e === "Терапевт") {
        selectOutput.insertAdjacentHTML("afterbegin", fildsTherapist);
      }
    }
  }

  function editFetch()
  {
    console.log("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" + tempId);
    const url = "https://ajax.test-danit.com/api/v2/cards/" + tempId;
    fetch(url, {
  method: 'DELETE',
  headers: {
    'Authorization': `Bearer ${token}`
  },
})
}
  