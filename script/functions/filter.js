const filterForm = document.querySelector('form');
const visitsBoard = document.getElementById('visitsBoard');
const filterInput = document.getElementById('search');
const filterStatus = document.getElementById('status');
const filterPriority = document.getElementById('priority');
const filterButton = document.getElementById('filter-button');

async function getData() {
    try {
        const response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error.message);
        throw error; 
    }
}

async function fetchData() {
    try {
        const data = await getData();
        localStorage.setItem('allVisits', JSON.stringify(data)) // явно переводимо масив візитів у строку інакше буде [object Object]
        // розбираємо строку з localStorage для перетворення у масив з об'єктами та записуємо результат у visitsCollection
        visitsCollection = JSON.parse(localStorage.getItem('allVisits'))
        console.log(visitsCollection);
        searchFilter(visitsCollection)
    } catch (error) {
        console.error(error.message);
    }
}

function noItems(array) {
    if(array.length === 0) {
        const noItemsElement = document.createElement('p');
        noItemsElement.innerText = 'Жодних візитів не було додано.'
        visitsBoard.insertAdjacentElement('beforeend', noItemsElement)
    }
}

const inputFieldFilter = (array) => {                // фільтрує масив відповідно до даних в текстовому полі
    if (filterInput.value === '') {                  // якщо в полі пусто, то повертає масив без змін
        return array;
    } else {                                        // перевіряємо, чи існує у картки title та description, чи вони є рядками перед тим, як відфільтрувати
        let filteredArr;
        filteredArr = array.filter(visit => 
            (visit.title && typeof visit.title === 'string' && visit.title.toLowerCase().includes(filterInput.value.toLowerCase())) || 
            (visit.description && typeof visit.description === 'string' && visit.description.toLowerCase().includes(filterInput.value.toLowerCase())) 
        );
        
        return filteredArr;
    }
}

const priorityFieldFilter = (array) => {             // фільтрує масив відповідно до даних в полі priory
    let filteredArr;
    if (filterPriority.value !== 'all') {                    
        filteredArr = array.filter(visit => visit.priory === filterPriority.value)
    } else {                                        // якщо нічого не вибрано, повертає масив без змін
        return array
    }
    return filteredArr;
}

const statusFieldFilter = (array) => {              // фільтрує масив відповідно до даних в полі status
    let filteredArr;
    if (filterStatus.value !== 'all') {
        filteredArr = array.filter(visit => visit.status === filterStatus.value)
    } else {                                        // якщо нічого не вибрано, повертає масив без змін
        return array
    }
    return filteredArr;
}

function searchFilter(array) {
    let filteredArr = array;                        
    visitsBoard.innerHTML = '';

    // перевіряємо кожне з полів щоразу як відбувається подія
    // таким чином всі раніше змінені значення залишаються
    filteredArr = inputFieldFilter(filteredArr);
    filteredArr = statusFieldFilter(filteredArr);
    filteredArr = priorityFieldFilter(filteredArr);

    noItems(filteredArr);
    console.table(filteredArr);
    render(filteredArr);
    // renderingCards(filteredArr) - тут має бути виклик функції для рендерингу карток

}

filterButton.addEventListener('click', (event) => {
    const isAuthorized = localStorage.getItem('authorized')
    if (isAuthorized === 'true') {
        event.preventDefault();
        fetchData();
    } else {
        alert('Ви не авторизувались!');
    }
})